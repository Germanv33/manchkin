import asyncio
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher, FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils import executor
from game_creater import user_add, add_user_connect, change_id
from aiogram.contrib.fsm_storage.memory import MemoryStorage


TOKEN = '1498460280:AAFaZBsJF5QY2hMhXvhAao4WTxN1T2owEv0'

#datas_for_bot
class AllData(StatesGroup):
    id_of_game = State()

bot = Bot(token=TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())
menu_markup = InlineKeyboardMarkup()
rules_button = InlineKeyboardButton('Правила', callback_data='rules_button')
create_game_button = InlineKeyboardButton('Создать игру', callback_data='create_game_button')
join_game_button = InlineKeyboardButton('Присоединиться', callback_data='join_game_button')
menu_markup.add(rules_button, create_game_button, join_game_button)


@dp.message_handler(commands=['start'])
async def process_start_command(message: types.Message):
    await bot.send_message(message.from_user.id,text='<b>Добро пожаловать в игру "Манчкин"!</b>\n'
                        'Для ознакомления с правилами игры нажмите <i>"Правила"</i>\n'
                        'Для создания игры нажмите <i>"Создать игру"</i>\n'
                        'Для присоединения к уже существующей игре нажмите <i>"Присоединиться"</i>\n',
                        reply_markup=menu_markup, parse_mode='HTML')

@dp.callback_query_handler(lambda c: c.data == 'rules_button')
async def callback_rules(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    await bot.send_message(callback_query.from_user.id,
                           r'Правила игры: https://nastolkoff.ru/kartochnye-igry/manchkin-kak-igrat')

@dp.callback_query_handler(lambda c: c.data == 'create_game_button')
async def callback_create_game(callback_query: types.CallbackQuery):
    new_game_id = InlineKeyboardMarkup()
    new_game_id_button = InlineKeyboardButton('Создать новую игру', callback_data='new_game')
    new_game_id.add(new_game_id_button)
    await bot.answer_callback_query(callback_query.id)
    user_name = callback_query.from_user.username
    if user_name == None:
        await bot.send_message(callback_query.from_user.id,
                               'Пожалуйста, поставьте юзернейм в телеграмме для успешной игры.\n'
                               'После этого повторите попытку создания игры.')
    else:
        await bot.send_message(callback_query.from_user.id, 'Ваш номер игры: <b>' + str(user_add(user_name)) +
                               '\n</b>Другие игроки должны ввести этот номер для подключения к вашей игре.'
                               '\nЕсли вам нужно поменять этот номер или это номер игры к которй вы уже присоеденены,'
                               'нажмите на кнопку <i>"Создать новую игру"</i>', parse_mode='HTML',
                               reply_markup=new_game_id)

@dp.callback_query_handler(lambda c: c.data == 'new_game')
async def change_game_id(callback_query: types.CallbackQuery):
    user_name = callback_query.from_user.username
    new_game_id = InlineKeyboardMarkup()
    new_game_id_button = InlineKeyboardButton('Создать новую игру', callback_data='new_game')
    new_game_id.add(new_game_id_button)
    await bot.send_message(callback_query.from_user.id, 'Ваш номер игры: <b>' + str(change_id(user_name)) +
                           '\n</b>Другие игроки должны ввести этот номер для подключения к вашей игре.'
                           '\nЕсли вам нужно поменять этот номер или это номер игры к которй вы уже присоеденены,'
                           'нажмите на кнопку <i>"Создать новую игру"</i>', parse_mode='HTML',
                           reply_markup=new_game_id)


@dp.callback_query_handler(lambda c: (c.data == 'join_game_button') or (c.data == 'new_attempt'), state="*")
async def callback_join_game(callback_query: types.CallbackQuery):
    await bot.send_message(callback_query.from_user.id, text='Введите номер игры, который дал вам создатель игры.')
    await AllData.id_of_game.set()


@dp.message_handler(state=AllData.id_of_game, content_types=types.ContentTypes.TEXT)
async def nex_step(message: types.Message, state=FSMContext):
    connect_markup = InlineKeyboardMarkup()
    new_attempt_button = InlineKeyboardButton('Повторить попытку', callback_data='new_attempt')
    connect_markup.add(new_attempt_button)
    game_id = message.text
    username = message.from_user.username
    if add_user_connect(username, game_id) == 'Success!':
        await bot.send_message(message.from_user.id, 'Подключение выполнено!')
    elif add_user_connect(username, game_id) == 'Bad':
        await bot.send_message(message.from_user.id, 'Такой игры не существует.\nПовторите попытку.',
                               reply_markup=connect_markup)
    elif add_user_connect(username, game_id) == 'User in game':
        await bot.send_message(message.from_user.id, 'Вы уже в игре.\nПожалуйста введите ваш номер игры.',
                               reply_markup=connect_markup)
    await state.finish()




















if __name__ == '__main__':
    executor.start_polling(dp)
