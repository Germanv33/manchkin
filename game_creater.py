import sqlite3
from random import randrange

db = sqlite3.connect('users.db')
sql = db.cursor()


def create_table():
	sql.execute("""CREATE TABLE IF NOT EXISTS users_and_games (
			tg_username TEXT,
			game_id INT,
			level INT,
			head_item TEXT,
			body_item TEXT,
			weapon TEXT,
			legs_item TEXT,
			win TEXT
		)""")
	db.commit()

def delete():
	sql.execute("DELETE FROM users_and_games")

def user_add(username):
	game_id = randrange(100000, 999999)
	level = 1
	head_item = None
	body_item = None
	weapon = None
	legs_item = None
	win = 0


	sql.execute(f"SELECT tg_username FROM users_and_games WHERE tg_username = '{username}'")
	if sql.fetchone() is None:
		sql.execute("INSERT INTO users_and_games VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
		 (username, game_id, level, head_item, body_item, weapon, legs_item, win))
		db.commit()
		print('All right!')
	else:
		pass
	for value in sql.execute(f"SELECT game_id FROM users_and_games WHERE tg_username = '{username}'"):
		return value[0]

def add_user_connect(username, game_id):
	level = 1
	head_item = None
	body_item = None
	weapon = None
	legs_item = None
	win = 0

	sql.execute(f"SELECT game_id FROM users_and_games WHERE game_id = '{game_id}'")
	if sql.fetchone() is None:
		for value in sql.execute("SELECT * FROM users_and_games"):
			print(value)
		return 'Bad'
	else:
		sql.execute(f"SELECT tg_username FROM users_and_games WHERE tg_username = '{username}'")
		if sql.fetchone() is None:
			sql.execute("INSERT INTO users_and_games VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
						(username, game_id, level, head_item, body_item, weapon, legs_item, win))
			db.commit()
			print('All right!')

		for value in sql.execute("SELECT * FROM users_and_games"):
			print(value)
		else:
			edit_player(username, game_id)

		return "Success!"

def edit_player(username, game_id):
	level = 1
	head_item = None
	body_item = None
	weapon = None
	legs_item = None
	win = 0
	sql.execute(f"DELETE FROM users_and_games WHERE tg_username = '{username}'")
	db.commit()
	sql.execute("INSERT INTO users_and_games VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
				(username, game_id, level, head_item, body_item, weapon, legs_item, win))
	db.commit()
	print('This func')

def change_id(username):
	game_id = randrange(100000, 999999)
	sql.execute(f"UPDATE users_and_games SET game_id = '{game_id}' WHERE tg_username = '{username}'")
	db.commit()
	for value in sql.execute(f"SELECT game_id FROM users_and_games WHERE tg_username = '{username}'"):
		return value[0]

for value in sql.execute("SELECT * FROM users_and_games"):
	print(value)
